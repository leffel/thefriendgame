# The Friend Game
**tfg** is a 2-player game where players get to know each other by taking turns asking and answering questions. it's built on on the  [Howdy BotKit library](https://github.com/howdyai/botkit-starter-slack/blob/master/bot.js).

here's how it works:

1. i ask my friend a question,
2. my friend answers it,
3. i answer my own question, and then
4. **tfg** reveals both answers at the same time!

## / setting it up /
currently, **tfg** can only be run locally and requires a considerable amount of setup.

here's how to do it:

1. clone this repo and `npm install`

2. set up a slackbot following [this guide](https://github.com/howdyai/botkit/blob/master/docs/slack-events-api.md). note: you dont need to do steps 5, 6, or 7. **tfg** doesn't use interactive buttons or the events api, and the login step isnt necessary. i used ngrok to tunnel out for my redirect url.

3. you *will* need to install the app under 'Features > OAuth & Permissions' on (this page)[https://api.slack.com/)] so you can generate a Bot User OAuth Access Token. you wont be able to do this until you set up your redirect url.

4. run this from the repo's directory, while you have ngrok open on the same port (eg. 3000):
```
clientId=<slackClientId> clientSecret=<slackClientSecret> PORT=<3000> token=<slackBotUserOAuthAccessToken> node tfg.js
```

5. go to the Slack team where you've installed your **tfg** bot - you should see it online!

## / how to play /

1. invite your bot to a slack team that you and your friends use

2. dm your bot with the word 'play'

3. after that, **tfg** will give you instructions as you go!

## / things to know /

all questions and answers will be stored locally. they are not encrypted or protected in any way!

also, the connection to the RTM will close if the bot doesnt receive any messages for a while. restart the server to keep playing.
