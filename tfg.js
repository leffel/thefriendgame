if (!process.env.clientId || !process.env.clientSecret || !process.env.PORT || !process.env.token) {
  console.log('Error: Specify clientId clientSecret slackToken and PORT in environment')
  usage_tip()
  process.exit(1)
}

const Botkit = require('botkit')
const debug = require('debug')('botkit:main')
const controller = Botkit.slackbot({
    clientId: process.env.clientId,
    clientSecret: process.env.clientSecret,
    debug: false,
    scopes: ['bot'],
    json_file_store: __dirname + '/.db/' // stores user data in a simple JSON format
    //include "log: false" to disable logging or a "logLevel" integer from 0 to 7 to adjust logging verbosity
})

const { hears, startTicking, storage: { channels } } = controller

controller.spawn({ token: process.env.token }).startRTM()

startTicking

hears(['play'], 'direct_message', (bot, message) => {
  const { user, channel, text } = message
  bot.startPrivateConversation(message, getPlayer2(bot, message))
})

controller.on('channel_joined', (bot, message) => {
  const { channel } = message
  bot.say({ text: "thanks for inviting me!", channel: channel})
})

hears(['start ([0-9]{4})'], 'ambient', (bot, message) => {
  bot.startConversation(message, startGame(bot, message))
})

hears(['accept ([0-9]{4})'], 'ambient', (bot, message) => {
  bot.startConversation(message, getConsent(bot, message))
})


function startGame(bot, message) {
  const { user, channel } = message
  const pin = message.match[1]

  return (err, convo) => {
    if (err) throw err

    bot.api.groups.info({ token: process.env.token, channel: channel }, (err, response) => {
      channels.get(pin, (err, data) => {
        if (err) {
          convo.say("sorry, that id is wack!")
        }
        else {
          var updateData = data
          var { players: { player1, player2 }, bot_id } = updateData
          var channel_has = response.group.members.sort().join()
          var channel_should_have = [player1.id, player2.id, bot_id].sort().join()

          if (channel_has == channel_should_have){
            updateData.shared_channel = channel

            if (player1.accepted && player2.accepted){
              convo.say("you're already playing that game...")
            } else if (player1.accepted && !player2.accepted) {
              convo.say("👋 hi there!")
              convo.say(`<@${player2.id}>` + ", say 'accept " + pin + "' to play The Friend Game with " + `<@${player1.id}>` + " 💁")
            }

            channels.save(updateData, (err) => { if (err) throw err })
          }
          else {
            convo.say("whoops, it looks you're not in the right room!")
          }
        }
      })
    })
  }
}

function getConsent(bot, message) {
  const { user, channel } = message
  const pin = message.match[1]

  return (err, convo) => {
    if (err) throw err

    bot.api.groups.info({ token: process.env.token, channel: channel }, (err, response) => {
      channels.get(pin, (err, data) => {
        if (err) {
          convo.say("sorry, that id is wack!")
        }
        else {
          var updateData = data
          var { players: { player1, player2 }, bot_id } = updateData
          var channel_has = response.group.members
          var channel_should_have = [player1.id, player2.id, bot_id]

          if (channel_has.sort().join() == channel_should_have.sort().join()){
            if (message.user == player2.id){
              player2.accepted = true;
              convo.say("great!")
              convo.say("asking " + `<@${player2.id}>` + " the question now!")
              bot.startPrivateConversation({ user: player2.id }, getPlayer2Answer(bot, message, pin))
            }
            else if (message.user == player1.id){
              convo.say("sorry " + `<@${player1.id}>` + ", you can't accept on behalf of " + `<@${player2.id}>` + "!")
            }
            channels.save(updateData, (err) => { if (err) throw err })
          }
          else {
            convo.say("whoops, it looks you're not in the right room!")
          }
        }
      })
    })
  }
}

function getPlayer2Answer(bot, message, pin) {
  return (err, convo) => {
    if (err) throw err

    channels.get(pin, (err, data) => {
      if (err) throw err

      var updateData = data
      var { players: { player1, player2 }, question } = updateData

      convo.say(`<@${player1.id}>` + ' asked, "' + question + '"')

      convo.ask('how would you like to answer?',
        [{ default: true,
           callback(response, convo) {
            player2.answer = convo.extractResponse('p2_answer')
            convo.say('thanks! your answer is: "' + player2.answer + '"')
            convo.next()
            channels.save(updateData, (err) => { if (err) throw err })
            bot.startPrivateConversation({ user: player1.id }, getPlayer1Answer(bot, pin))
          }
        }], { key: 'p2_answer' })
    })
  }
}

function getPlayer1Answer(bot, pin) {
  return (err, convo) => {
    if (err) throw err

    channels.get(pin, (err, data) => {
      if (err) throw err
      var updateData = data
      var { players: { player1, player2 }, question, shared_channel } = updateData

      convo.say(`<@${player2.id}>` + ' just answered your question!')
      convo.say('you originally asked, "' + question + '"')

      convo.ask('how would you like to answer?', [{default: true, callback(response, convo){
        player1.answer = convo.extractResponse('p1_answer')
        convo.say('your answer is: ' + player1.answer)
        convo.next()
        channels.save(updateData, (err) => {
          if (err) throw err
          bot.startConversation({ channel: shared_channel }, endTheRound(bot, pin))
        })
      }}], { key: 'p1_answer'})
    })
  }
}

function endTheRound(bot, pin) {
  return (err, convo) => {
    if (err) throw err
    channels.get(pin, (err, data) => {
      var { players: { player1, player2 }, question } = data
      convo.say('the results are in!')
      convo.say(`<@${player1.id}>` + ' asked, ' + question)
      convo.say(`<@${player2.id}>` + ' answered, ' + player2.answer)
      convo.say(`<@${player1.id}>` + ' answered, ' + player1.answer)
      convo.say('want to play again? DM me and say "play"')
    })
  }
}


function getPlayer2(bot, message) {
  const { user, channel } = message

  return (err, convo) => {
    if (err) throw err
    convo.ask('hi! who do you want to play with?', [
      {
        default: true,
        callback(response, convo) {
          const userData = response.text.match(/<@([A-Z0-9]{9})>/) // todo: use botkit pattern matching instead?
          if (userData) {
            getQuestion(response, convo, message)
            convo.next()
          }
          else {
            convo.say('woops, that\'s not right! just @ your friend :)')
            convo.repeat()
            convo.next()
          }
        }
      }
    ], { key: 'player2' } )
  }
}

function getQuestion(response, convo, message) {
  var player2 = convo.extractResponse('player2')
  convo.ask('okay! what do you want to ask ' + player2 + ' ?' , [
    {
      default: true,
      callback(response, convo) {
        createGameData(response, convo, message)
        convo.next()
      }
    }
  ], { key: 'question' } )
}

function createGameData(response, convo, message){
  var player1 = message.user
  var p1_dm_channel = message.channel
  var player2 = convo.extractResponse('player2').match(/([A-Z0-9]{9})/)[0]
  var question = convo.extractResponse('question')
  var id = createGamePin()

  const gameData = {
    id: id,
    bot_id: 'U4695U6DT',
    shared_channel: '',
    players: {
      player1: {
        id: player1,
        accepted: true,
        played: '',
        dm_channel: p1_dm_channel,
        answer: ''
      },
      player2: {
        id: player2,
        accepted: false,
        played: '',
        dm_channel: '',
        answer: ''
      }
    },
    question: question
  }

  channels.save(gameData, function(err) {
    giveInstructions(response, convo, message, gameData.id)
  })
}

function giveInstructions(response, convo, message, pin) {
  channels.get(pin, function(err, gameData){
    var pin = gameData.id
    var player2 = gameData.players.player2
    var question = gameData.question
    convo.say('alright, got it. you want to ask ' + `<@${player2.id}>` + ', \'' + question + '\'')
    convo.say('invite me to a shared DM channel with ' + `<@${player2.id}>` + ' and say \'start ' + pin + '\'')
  })
}

function createGamePin(){
  // todo: don't repeat pins!!
  return Math.floor(1000 + Math.random() * 1000).toString()
}
